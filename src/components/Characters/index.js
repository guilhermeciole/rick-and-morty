import "./style.css";

function Characters({ characterList }) {
  return (
    <>
      <div className="titulo">
        <h1>Meus personagens</h1>
      </div>
      <div className="container">
        {characterList.map((item) =>
          item.status === "Alive" ? (
            <div key={item.id} className="personagem-vivo">
              <p className="nome">{item.name}</p>
              <p>{item.species}</p>
              <img src={item.image}></img>
            </div>
          ) : (
            <div key={item.id} className="personagem-nao-vivo">
              <p className="nome">{item.name}</p>
              <p>{item.species}</p>
              <img src={item.image}></img>
            </div>
          )
        )}
      </div>
    </>
  );
}

export default Characters;
